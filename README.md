# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 
![](https://i.imgur.com/ddmVgMz.png)
This button is to upload files.
![](https://i.imgur.com/yDx8rut.png)
This button is to downlad files.
![](https://i.imgur.com/ugzJZJo.png)
This button is to input text. Then input box will be represent when you click the canvas.
![](https://i.imgur.com/RUKyszL.png)
This button is to reser canvas.
![](https://i.imgur.com/9FMcgQ3.png)
This button is to restore the states of canvas.
![](https://i.imgur.com/PwbYWmU.png)
When you push undo ,you can push this button to redo.
![](https://i.imgur.com/zJFIEJd.png)
This button is a pen.
![](https://i.imgur.com/vY7lIGd.png)
You can erase things on the canvas when you push this button.
![](https://i.imgur.com/1Kl1OTT.png)
This button is to draw straight line.
![](https://i.imgur.com/rpBtYYp.png)
This button is to draw traingle.
![](https://i.imgur.com/zpdmeDy.png)
This button is to draw rectangle.
![](https://i.imgur.com/tPkKROJ.png)
This button is to draw circle.


### Function description

![](https://i.imgur.com/8SvoHio.png)
This function is to handle the react of mousemove. The if-else have different condition for pen, eraser...etc.
![](https://i.imgur.com/J6JYD8v.png)
This function is to catch the event of mousedown, and it has two states for draw and text.
![](https://i.imgur.com/pXSPdfm.png)
These two function is to do undo and redo, I store the img of canvas  and push into an array, then call it when I use these two function.
![](https://i.imgur.com/aM5is5M.png)
These two function is to read the file I upload. The first function is to turn it to base64 type. The second one is to draw on the canvas.
### Gitlab page link

    https://107062339.gitlab.io/canvas/

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>